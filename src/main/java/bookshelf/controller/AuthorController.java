package bookshelf.controller;


import bookshelf.domain.Author;
import bookshelf.domain.Book;
import bookshelf.repository.AuthorRepository;
import bookshelf.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AuthorController {
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private BookRepository bookRepository;

    @RequestMapping(path = "/allAuthors", method = RequestMethod.GET)
    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    @RequestMapping(path = "/author/{id}", method = RequestMethod.GET)
    public Author getAuthor(@PathVariable("id") Long id) {
        return authorRepository.findOne(id);
    }

    @RequestMapping(path = "/author", method = RequestMethod.POST)
    public ResponseEntity<Author> addAuthor(@RequestBody Author author) {
        author = authorRepository.save(author);
        return new ResponseEntity<>(author, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/author/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Author> updateAuthor(@PathVariable("id") Long id, @RequestBody Author author) {
        author.setId(id);
        author = authorRepository.save(author);
        return new ResponseEntity<>(author, HttpStatus.OK);
    }

    @RequestMapping(path = "/author/{id}/books", method = RequestMethod.GET)
    public List<Book> findAuthors(@PathVariable("id") Long id) {
        return bookRepository.findByAuthors_Id(id);
    }
}
