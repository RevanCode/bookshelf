package bookshelf.controller;

import bookshelf.domain.Book;
import bookshelf.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {
    @Autowired
    private BookRepository bookRepository;

    @RequestMapping(path = "/book/{id}", method = RequestMethod.GET)
    public Book getBook(@PathVariable("id") Long id) {
        return bookRepository.findOne(id);
    }

    @RequestMapping(path = "/book", method = RequestMethod.POST)
    public ResponseEntity<Book> addBook(@RequestBody Book book) {
        book = bookRepository.save(book);
        return new ResponseEntity<>(book, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/book/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Book> updateBook(@PathVariable("id") Long id, @RequestBody Book book) {
        book.setId(id);
        book = bookRepository.save(book);
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @RequestMapping(path = "/book", method = RequestMethod.GET)
    public List<Book> findBooks(@RequestParam(value = "title") String title) {
        return bookRepository.findByTitle(title);
    }
}
