package bookshelf;

import bookshelf.domain.Author;
import bookshelf.domain.Book;
import bookshelf.repository.BookRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BookControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private BookRepository bookRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private Book oldBook = new Book("Old book", "The book about ancient ages");
    private Book modernBook = new Book("Modern book", "The book about modern time");

    @Before
    public void setup() {
        oldBook = bookRepository.save(oldBook);
    }

    @After
    public void clear() {
        bookRepository.deleteAll();
    }

    @Test
    public void getBookById() throws Exception {
        String result = objectMapper.writeValueAsString(oldBook);

        mockMvc.perform(get("/book/" + oldBook.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(result));
    }

    @Test
    public void addNewBook() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(modernBook);

        String responseContent = mockMvc
                .perform(post("/book").contentType(MediaType.APPLICATION_JSON).content(jsonBody))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        Book responseBook = objectMapper.readValue(responseContent, Book.class);

        Book createdBook = bookRepository.findOne(responseBook.getId());

        Assert.assertEquals(createdBook.getSummary(), modernBook.getSummary());
        Assert.assertEquals(createdBook.getTitle(), modernBook.getTitle());
    }
    @Test
    public void addNewBookWithAuthor() throws Exception {
        Author expectedAuthor = new Author("Donald", "Goodman");
        modernBook.getAuthors().add(expectedAuthor);

        String jsonBody = objectMapper.writeValueAsString(modernBook);

        String responseContent = mockMvc
                .perform(post("/book").contentType(MediaType.APPLICATION_JSON).content(jsonBody))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        Book responseBook = objectMapper.readValue(responseContent, Book.class);

        Book createdBook = bookRepository.findOne(responseBook.getId());

        Assert.assertEquals(createdBook.getSummary(), modernBook.getSummary());
        Assert.assertEquals(createdBook.getTitle(), modernBook.getTitle());

        Author resultAuthor = createdBook.getAuthors().stream().findFirst().get();

        Assert.assertEquals(expectedAuthor.getFirstName(), resultAuthor.getFirstName());
        Assert.assertEquals(expectedAuthor.getLastName(), resultAuthor.getLastName());
    }

    @Test
    public void updateBook() throws Exception {
        final String updatedSummary = "new Summary";

        oldBook.setSummary(updatedSummary);
        String jsonBody = objectMapper.writeValueAsString(oldBook);

        String responseContent = mockMvc
                .perform(put("/book/" + oldBook.getId()).contentType(MediaType.APPLICATION_JSON).content(jsonBody))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        Book responseBook = objectMapper.readValue(responseContent, Book.class);

        Book createdBook = bookRepository.findOne(responseBook.getId());

        Assert.assertEquals(createdBook.getSummary(), updatedSummary);
        Assert.assertEquals(createdBook.getTitle(), oldBook.getTitle());
    }


    @Test
    public void getBookByTitle() throws Exception {
        String result = objectMapper.writeValueAsString(Collections.singleton(oldBook));

        mockMvc.perform(get("/book").param("title", oldBook.getTitle()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(result));
    }

    @Test
    public void getBookByNullTitle() throws Exception {
        mockMvc.perform(get("/book"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
    @Test
    public void getBookByEmptyTitle() throws Exception {
        String result = objectMapper.writeValueAsString(Collections.emptyList());

        mockMvc.perform(get("/book").param("title", ""))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(result));

    }


}
