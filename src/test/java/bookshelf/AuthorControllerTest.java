package bookshelf;

import bookshelf.domain.Author;
import bookshelf.domain.Book;
import bookshelf.repository.AuthorRepository;
import bookshelf.repository.BookRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthorControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private BookRepository bookRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private Author oldAuthor = new Author("John", "Classic");
    private Author modernAuthor = new Author("David", "Modern");

    @Before
    public void setup() {
        oldAuthor = authorRepository.save(oldAuthor);
    }

    @After
    public void clear() {
        authorRepository.deleteAll();
    }

    @Test
    public void getAuthorById() throws Exception {
        String result = objectMapper.writeValueAsString(oldAuthor);

        mockMvc.perform(get("/author/" + oldAuthor.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(result));
    }

    @Test
    public void addNewAuthor() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(modernAuthor);

        String responseContent = mockMvc
                .perform(post("/author").contentType(MediaType.APPLICATION_JSON).content(jsonBody))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        Author responseAuthor = objectMapper.readValue(responseContent, Author.class);

        Author createdAuthor = authorRepository.findOne(responseAuthor.getId());

        Assert.assertEquals(createdAuthor.getFirstName(), modernAuthor.getFirstName());
        Assert.assertEquals(createdAuthor.getLastName(), modernAuthor.getLastName());
    }

    @Test
    public void updateAuthor() throws Exception {
        final String updatedLastName = "new LastName";

        oldAuthor.setLastName(updatedLastName);
        String jsonBody = objectMapper.writeValueAsString(oldAuthor);

        String responseContent = mockMvc
                .perform(put("/author/" + oldAuthor.getId()).contentType(MediaType.APPLICATION_JSON).content(jsonBody))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        Author responseAuthor = objectMapper.readValue(responseContent, Author.class);

        Author createdAuthor = authorRepository.findOne(responseAuthor.getId());

        Assert.assertEquals(createdAuthor.getFirstName(), oldAuthor.getFirstName());
        Assert.assertEquals(createdAuthor.getLastName(), updatedLastName);
    }

    @Test
    public void getAuthorBooks() throws Exception {

        Book book = new Book("Old book", "The book about ancient ages");
        bookRepository.save(book);

        book.getAuthors().add(oldAuthor);
        book = bookRepository.save(book);

        String result = objectMapper.writeValueAsString(Collections.singleton(book));

        mockMvc.perform(get("/author/" + oldAuthor.getId() + "/books"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(result));
    }
}
