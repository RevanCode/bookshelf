# BOOKSHELF #

The application with REST API for managing info about books and their authors.

### Run ###

    gradlew bootRun

### Test ###

    gradlew test